# Rest Api Form

Backend salvataggio dati




### Requisiti

  - [Node Js][nodejs]
  - [firebase-tools][firebase]
  - [Postman][postman]

### How to

```sh
$ git clone git@bitbucket.org:Gigiz/rest-api-form.git
$ cd rest-api-form
$ cd functions
$ npm install
$ cd ..
$ firebase serve --only functions,hosting
```
#### GET

Per recuperare i dati precedentemente salvati

In locale:  http://localhost:5001/rest-api-form/us-central1/app
Produzione: https://us-central1-rest-api-form.cloudfunctions.net/app/api/v1/apply

#### POST


[postman]: <https://www.getpostman.com/>
[nodejs]: <https://nodejs.org/it/>
[firebase]: <https://github.com/firebase/firebase-tools>