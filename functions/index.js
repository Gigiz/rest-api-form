const functions = require('firebase-functions');
const firebase = require('firebase-admin');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const morgan = require('morgan');
//const firebase = require('firebase');
//const port = process.env.PORT || 9001; 

const firebaseApp = firebase.initializeApp(
	functions.config().firebase
);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({limit: '50mb'}));
app.use(morgan('dev'));

app.use((req, res, next) => {
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Access-Control-Allow-Methods', 'GET, POSTS');
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, Accept');
	next();
});

app.get('/', (req, res) => res.send('Rest Api Application Form'));

/*const config = {
  apiKey: 'AIzaSyCnoGb6I1eDNgRn9NNNWNG8ufR45iym8Ss',
  authDomain: 'rest-api-form.firebaseapp.com',
  databaseURL: 'https://rest-api-form.firebaseio.com',
  projectId: 'rest-api-form',
  storageBucket: 'rest-api-form.appspot.com',
  messagingSenderId: '222174462223'
};
firebase.initializeApp(config);*/

const db = firebaseApp.database();
const candidatesRef = db.ref('candidates');

const apiRouter = express.Router();
apiRouter.use( (req, res, next) => next() );
apiRouter.get('/', (req, res) => res.json({ message: 'ok'}) );

apiRouter.route('/v1/apply')

	.post((req, res) => {
    //console.log(req.body);
    let applyObj = req.body;
		candidatesRef.push(applyObj, err => {
			if (err) {
				res.send(err)
			} else {
				res.json({ message: 'Success: User created.'})
			}
    });

	})
	.get((req, res) => {
		candidatesRef.once('value', snapshot => res.json(snapshot.val()) );
	});

app.use('/api', apiRouter);

//app.listen(port);
//console.log('server listening port: ' + port);

exports.app = functions.https.onRequest(app);